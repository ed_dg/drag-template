import React, { useState } from "react";
import CompItemTypes from "../helpers/ItemTypes";
import { Card, CardContent, Divider, Grid } from "@material-ui/core";
import TitleIcon from "@material-ui/icons/Title";
import TextFormatIcon from "@material-ui/icons/TextFormat";
import YouTubeIcon from "@material-ui/icons/YouTube";
import ImageIcon from "@material-ui/icons/Image";
import { Box, TargetLayout } from "../components";

import DeleteContext from "./Context/DeleteContext";
import UpdateContext from "./Context/UpdateContext";
import MainDragCompStyles from "./MainDragCompStyles";

const Container = () => {
  const classes = MainDragCompStyles();

  const [targetLayout, setTargetLayout] = useState([
    {
      accepts: [
        CompItemTypes.TEXT,
        CompItemTypes.IMAGE,
        CompItemTypes.YOUTUBE,
        CompItemTypes.TITLE,
        CompItemTypes.ENDPOINT
      ],
      storedComponents: []
    }
  ]);

  const [components] = useState([
    {
      name: "Title",
      type: CompItemTypes.TITLE,
      ukey: "",
      value: "",
      icon: <TitleIcon />
    },
    {
      name: "Text",
      type: CompItemTypes.TEXT,
      ukey: "",
      value: "",
      icon: <TextFormatIcon />
    },
    {
      name: "Youtube Video",
      type: CompItemTypes.YOUTUBE,
      ukey: "",
      value: "",
      icon: <YouTubeIcon />
    },
    {
      name: "Image",
      type: CompItemTypes.IMAGE,
      ukey: "",
      value: "",
      icon: <ImageIcon />
    }
  ]);

  // Handle component drop on selected target (add item to array)
  const handleDrop = (index, component) => {
    /* Get current object component from component list.  */
    let filtered = components.find(obj => obj.type === component.type);
    // TODO: Fix Index to be able to handle ID unique on array here.
    /* update the component ukey with some unique value */
    let total = targetLayout.length+1;
    console.log(total);
    filtered.ukey = `${component.type}__${total}`;
    console.log(filtered.ukey);

    /* Set value to the component list back */
    let currentLayout = targetLayout[index].storedComponents;
    currentLayout.push(filtered);
    setTargetLayout(targetLayout);
  };

  // Handle component editing
  const handleUpdate = (index, value) => {
    console.log(`[handleUpdate]->index(${index})->value(${value})`);
    // let targetCopy = targetLayout[0];
    // let components = targetCopy.storedComponents;
    // components[index].value = value;
    // targetCopy.storedComponents = components;
    // setTargetLayout([targetCopy]);
    // console.log(targetCopy);
  };

  // Handle Deletion of component inside lastDroppedCompItem current index
  const handleDelete = index => {
    console.log(`[handleDelete]->index(${index})`);

    let targetCopy = targetLayout[0];
    let components = targetCopy.storedComponents;
    delete components[index];
    targetCopy.storedComponents = components;
    setTargetLayout([targetCopy]);
    console.log(targetCopy);
  };

  return (
    <Grid container spacing={2} className={classes.grid}>
      <DeleteContext.Provider value={handleDelete}>
        <UpdateContext.Provider value={handleUpdate}>
          <Grid item xs={3}>
            <Card>
              <CardContent>
                <h3 className={classes.h3}>Components</h3>
                <div className={classes.small}>
                  Please drag and drop to layout your components
                </div>
                <Divider />
                {components.map(({ name, type, icon }, index) => (
                  <Box name={name} type={type} key={index} icon={icon} />
                ))}
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={9}>
            <Card>
              <CardContent>
                <h3 className={classes.h3}>Layout</h3>
                <div className={classes.small}>
                  Please drop on top of your preferred layout your components
                </div>
                <Divider />

                <div key={"target"}>
                  <TargetLayout
                    compID={0}
                    accept={targetLayout[0].accepts}
                    storedComponents={targetLayout[0].storedComponents}
                    onDrop={item => handleDrop(0, item)}
                    key={`target_layout_${0}`}
                  />
                </div>
              </CardContent>
            </Card>
          </Grid>
        </UpdateContext.Provider>
      </DeleteContext.Provider>
    </Grid>
  );
};
export default Container;
