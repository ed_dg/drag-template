import { makeStyles } from "@material-ui/core/styles";

const MainDragCompStyles = makeStyles(theme => ({
  h3: {
    color: "#4B597A",
    marginBottom: "5px"
  },
  app: {
    backgroundColor: "#F6F5F8"
  },
  formatCard: {
    backgroundColor: "#F8F9FB",
    padding: "20px",
    marginTop: "15px",
    cursor: "pointer",
    borderStyle: "dashed",
    borderColor: "#D6D7D7",
    border: "2px",
    boxShadow: "none",
    height: "120px"
  },
  grid: {
    padding: "20px",
    width: "100%"
  },
  small: {
    color: "#CDCDCD",
    marginBottom: "15px",
    marginUp: "15px"
  },
  formatCardTarget: {
    backgroundColor: "#F8F9FB",
    padding: "20px",
    marginTop: "15px",
    cursor: "pointer",
    borderStyle: "dashed",
    borderColor: "#D6D7D7",
    border: "2px",
    boxShadow: "none",
    height: "120px"
  },
  gridsDrag: {
    borderStyle: "dashed",
    borderColor: "#D6D7D7",
    padding: "10px",
    border: "1px"
  }
}));

export default MainDragCompStyles;
