import React, { useState } from "react";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import ContentEditable from "react-contenteditable";
import TextStyles from "./TextStyles";

const Text = ({ id, onDelete }) => {
  const classes = TextStyles();
  const [value, setValue] = useState("Your text content here.");


  if (!id) {
    return <div>No ID</div>;
  } else if (!onDelete) {
    return <div>No Delete Function</div>;
  }


  const handleChange = event => {
    console.log(value);
    setValue(event.target.value);
  };

  return (
    <div key={`title_component_${id}`}>
      <div className={classes.EditBorder}>
        <div className={classes.EditHead}>
          <DragIndicatorIcon className={classes.iconColor} />{" "}
          <span className={classes.EditTitle}>Text</span>
          <HighlightOffIcon onClick={() => onDelete(id)} />
        </div>
        <div className={classes.h1Style}>
          <ContentEditable
            key={`editable_title_component_${id}`}
            html={value}
            disabled={false}
            className={classes.inputStyle}
            onChange={val => handleChange(val)}
          />
        </div>
      </div>
    </div>
  );
};
export default Text;
