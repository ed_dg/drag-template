import React from "react";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import ImageStyles from "./ImageStyles";

const Image = ({ id, onDelete }) => {
  const classes = ImageStyles();

  if (!id) {
    return <div>No ID</div>;
  } else if (!onDelete) {
    return <div>No Delete Function</div>;
  }

  let imgURL =
    "https://techcrunch.com/wp-content/uploads/2020/01/Busuu-Verbling-Team-Mikael-CEO-of-Verbling-Bernhard-CEO-of-Busuu-Gustav-CTO-of-Verbling.jpg?w=1390&crop=1";

  return (
    <div key={`image_component_${id}`}>
      <div className={classes.EditBorder}>
        <div className={classes.EditHead}>
          <DragIndicatorIcon className={classes.iconColor} />{" "}
          <span className={classes.EditTitle}>Image</span>
          <HighlightOffIcon onClick={() => onDelete(id)} />
        </div>
        <div style={{ textAlign: "center" }}>
          <img src={imgURL} alt={""} className={classes.ImageStyle} />
        </div>
      </div>
    </div>
  );
};
export default Image;
