import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { Image } from "../../components";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders with or without a name", () => {
  act(() => {
    render(<Image />, container);
  });
  expect(container.textContent).toBe("No ID");

  act(() => {
    render(<Image id={1} />, container);
  });
  expect(container.textContent).toBe("No Delete Function");
});
