import React from "react";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import YoutubeStyles from "./YoutubeStyles";

const Youtube = ({ id, onDelete }) => {
  if (!id) {
    return <div>No identifier</div>;
  } else if (!onDelete) {
    return <div>No Delete Function</div>;
  }

  const classes = YoutubeStyles();

  let videoURL = "https://www.youtube.com/embed/Lnrb8HnQvfU";

  return (
    <div key={`video_component_${id}`}>
      <div className={classes.EditBorder}>
        <div className={classes.EditHead}>
          <DragIndicatorIcon className={classes.iconColor} />
          <span className={classes.EditTitle}>Youtube Video</span>
          <HighlightOffIcon onClick={() => onDelete(id)} />
        </div>
        <iframe
          width="100%"
          height="315"
          title={"Video Element"}
          src={videoURL}
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>
    </div>
  );
};
export default Youtube;
