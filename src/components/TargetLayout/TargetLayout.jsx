import React, {useEffect}  from "react";
import { useDrop } from "react-dnd";
import { Card } from "@material-ui/core";
import FlipToFrontIcon from "@material-ui/icons/FlipToFront";
import PanToolIcon from "@material-ui/icons/PanTool";
import ItemTypes from "../../helpers/ItemTypes";
import { Text, Image, Title, Youtube } from "../../components";
import DeleteContext from "../../views/Context/DeleteContext";
import UpdateContext from "../../views/Context/UpdateContext";
import TargetLayoutStyles from "./TargetLayoutStyles";

const TargetLayout = ({ compID, accept=["TEXT"], storedComponents=[], onDrop={} }) => {

  const classes = TargetLayoutStyles();


  useEffect(function persistForm() {
    // 👍 We're not breaking the first rule anymore
    if (!compID) {
      return <div>No compID</div>;
    } else if (!accept) {
      return <div>No Accept Array</div>;
    } else if (!storedComponents) {
      return <div>No storedComponents Data</div>;
    } else if (!onDrop) {
      return <div>No onDrop function</div>;
    }

  });

  const [{ isOver, canDrop }, drop] = useDrop({
    accept,
    drop: onDrop,
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    })
  });



  const isActive = isOver && canDrop;

  let borderColor = "#D6D7D7";
  let backgroundColor = "#F8F9FB";

  if (isActive) {
    // hover
    borderColor = "#2853FF";
    backgroundColor = "#E8F0FE";
  } else if (canDrop) {
    borderColor = "#2853FF";
  }

  const dropState = (isActive, accept, classes) => {
    if (isActive) {
      return (
        <div className={classes.stateText}>
          <PanToolIcon />
          <span className={classes.verticalAlign}>Release to Drop</span>
        </div>
      );
    }
    return (
      <div className={classes.stateText}>
        <FlipToFrontIcon />{" "}
        <span className={classes.verticalAlign}>
          {`Accepts: ${accept.join(", ")} components`}
        </span>
      </div>
    );
  };

  const componentDrop = (type, index, typeList, onDelete, onUpdate) => {
    if (type === typeList.YOUTUBE) {
      return <Youtube key={`yt__${index}`} id={index} onDelete={onDelete} />;
    } else if (type === typeList.TEXT) {
      return (
        <Text
          key={`txt__${index}`}
          id={index}
          onDelete={onDelete}
          onUpdate={onUpdate}
        />
      );
    } else if (type === typeList.IMAGE) {
      return <Image key={`img__${index}`} id={index} onDelete={onDelete} />;
    } else if (type === typeList.TITLE) {
      return (
        <Title
          key={`ttl__${index}`}
          identifier={index}
          onDelete={onDelete}
          onUpdate={onUpdate}
        />
      );
    }
  };

  return (
    <Card
      key={`card_${compID}`}
      className={classes.formatCard}
      ref={drop}
      style={{
        borderColor: borderColor,
        backgroundColor: backgroundColor
      }}
    >
      {dropState(isActive, accept, classes)}

      <DeleteContext.Consumer>
        {onDelete => (
          <UpdateContext.Consumer>
            {onUpdate => (
              <div>
                {storedComponents.map(({ type }, index) => (
                  <div key={`component_${index}`}>
                    {componentDrop(type, index, ItemTypes, onDelete, onUpdate)}
                  </div>
                ))}
              </div>
            )}
          </UpdateContext.Consumer>
        )}
      </DeleteContext.Consumer>
    </Card>
  );
};
export default TargetLayout;
