import { makeStyles } from "@material-ui/core/styles";

const TargetLayoutStyles = makeStyles(theme => ({
  formatCard: {
    backgroundColor: "#F8F9FB",
    padding: "20px",
    marginTop: "15px",
    cursor: "pointer",
    borderStyle: "dashed",
    borderColor: "#D6D7D7",
    border: "2px",
    boxShadow: "none",
    minHeight: "120px",
    minWidth: "200px"
  },
  grids: {
    borderStyle: "dashed",
    borderColor: "#D6D7D7",
    padding: "10px",
    border: "1px"
  },
  stateText: {
    paddingBottom: "20px",
    fontWeight: "bold",
    fontSize: "18px"
  },
  verticalAlign: {
    position: "relative",
    top: "-5px",
    left: "10px",
    fontSize: "14px"
  }
}));

export default TargetLayoutStyles;
