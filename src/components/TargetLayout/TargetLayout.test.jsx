import React from "react";
import {render, unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import {TargetLayout} from "../../components";

let container = null;
beforeEach(() => {
    // setup a DOM element as a render target
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // cleanup on exiting
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders with or without a name", () => {
    act(() => {
        render(<TargetLayout/>, container);
    });
    expect(container.textContent).toBe("No compID");

    act(() => {
        render(<TargetLayout compID={1}/>, container);
    });
    expect(container.textContent).toBe("No Accept Array");

    act(() => {
        render(<TargetLayout compID={1} accept={["TEXT"]}/>, container);
    });
    expect(container.textContent).toBe("No storedComponents Data");

    act(() => {
        render(
            <TargetLayout
                compID={1}
                accept={["TEXT"]}
                storedComponents={[{key: 1}]}
            />,
            container
        );
    });
    expect(container.textContent).toBe("No onDrop function");
});
