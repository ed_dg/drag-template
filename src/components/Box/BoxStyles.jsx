import { makeStyles } from "@material-ui/core/styles";

const BoxStyles = makeStyles(theme => ({
  formatCard: {
    backgroundColor: "#FFF",
    padding: "10px",
    marginTop: "15px",
    cursor: "pointer",
    borderRadius: "5px",
    border: "2px solid #E7ECF1"
  },
  verticalAlign: {
    position: "relative",
    top: "-5px",
    left: "10px"
  },
  iconColor: {
    color: "#C2CDD9"
  }
}));

export default BoxStyles;
