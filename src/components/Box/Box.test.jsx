import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { Box } from "../../components";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders with or without a name", () => {
  act(() => {
    render(<Box />, container);
  });
  expect(container.textContent).toBe("No Name");

  act(() => {
    render(<Box name="title" icon={<DragIndicatorIcon />} />, container);
  });
  expect(container.textContent).toBe("No Type");

  act(() => {
    render(<Box name="title" type={"TEXT"} />, container);
  });
  expect(container.textContent).toBe("No Icon");
});
