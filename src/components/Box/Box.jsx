import React, {useEffect} from "react";
import { useDrag } from "react-dnd";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import BoxStyles from "./BoxStyles";

const Box = ({ name, type, icon }) => {
  const classes = BoxStyles();

  useEffect(function persistForm() {
    // 👍 We're not breaking the first rule anymore
    if (!name) {
      return <div>No Name</div>;
    } else if (!type) {
      return <div>No Type</div>;
    } else if (!icon) {
      return <div>No Icon</div>;
    }
  });


  /* Detect user behavior when dragging object */
  const [{ opacity }, drag] = useDrag({
    item: { name, type },
    collect: monitor => ({
      opacity: monitor.isDragging() ? 0.4 : 1
    })
  });

  return (
    <div ref={drag} style={{ opacity: opacity }} className={classes.formatCard}>
      <DragIndicatorIcon className={classes.iconColor} />
      {icon}
      <span className={classes.verticalAlign}>{name}</span>
    </div>
  );
};
export default Box;
