import {makeStyles} from "@material-ui/core/styles";

const TitleStyles = makeStyles(theme => ({
    EditBorder: {
        cursor: "pointer",
        borderRadius: "5px",
        border: "3px solid #5374FE",
        padding: "0px",
        // display: "inline-flex",
        // minWidth: "310px",
        position: "relative",
        marginTop: "35px"
    },
    EditHead: {
        position: "absolute",
        top: "-30px",
        left: "-3px",
        color: "white",
        padding: "4px 4px 2px 3px",
        backgroundColor: "#5374FE",
        borderTopLeftRadius: "5px",
        borderTopRightRadius: "5px",
        display: "inline-table"
    },
    EditTitle: {
        position: "relative",
        top: "-6px",
        left: "-2px",
        marginRight: "10px"
    },
    inputStyle: {
        outline: "none"
    },
    h1Style: {
        margin: "7px 7px 7px 7px"
    }
}));

export default TitleStyles