import React, { useState } from "react";
import DragIndicatorIcon from "@material-ui/icons/DragIndicator";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import ContentEditable from "react-contenteditable";
import TitleStyles from "./TitleStyles";

const Title = ({ identifier, onDelete, onUpdate }) => {
  const [value, setValue] = useState("Your title text here.");

  if (!identifier) {
    return <div>No identifier</div>;
  } else if (!onDelete) {
    return <div>No Delete Function</div>;
  } else if (!onUpdate) {
    return <div>No Update Function</div>;
  }

  const classes = TitleStyles();

  const handleChange = event => {
    console.log(value);
    setValue(event.target.value);
    onUpdate(identifier, value);
  };

  return (
    <div key={`title_${identifier}`}>
      <div className={classes.EditBorder}>
        <div className={classes.EditHead}>
          <DragIndicatorIcon className={classes.iconColor} />{" "}
          <span className={classes.EditTitle}> Title {identifier}</span>
          <HighlightOffIcon onClick={() => onDelete(identifier)} />
        </div>

        <h1 className={classes.h1Style}>
          <ContentEditable
            key={`editable_${identifier}`}
            defaultValue={"Your title text here"}
            html={value}
            inputMode={"text"}
            disabled={false}
            className={classes.inputStyle}
            onChange={val => handleChange(val)}
          />
        </h1>
      </div>
    </div>
  );
};
export default Title;
