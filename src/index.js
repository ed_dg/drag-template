import React from "react";
import ReactDOM from "react-dom";
import Container from "./views/Container";
import {DndProvider} from "react-dnd";
import Backend from "react-dnd-html5-backend";
import indexStyles from "./indexStyles";

function App() {
    const classes = indexStyles();

    return (
        <div className={classes.app}>
            <React.Fragment>
                <DndProvider backend={Backend}>
                    <Container/>
                </DndProvider>
            </React.Fragment>
        </div>
    );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App/>, rootElement);
