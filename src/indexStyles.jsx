import {makeStyles} from "@material-ui/core/styles";

const indexStyles = makeStyles(theme => ({
    app: {
        backgroundColor: "#F6F5F8"
    },
    grid: {
        padding: "20px"
    }
}));

export default indexStyles;