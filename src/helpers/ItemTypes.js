export default {
  YOUTUBE: "youtube",
  TEXT: "text",
  IMAGE: "image",
  TITLE: "title",
  ENDPOINT: "endpoint"
};
